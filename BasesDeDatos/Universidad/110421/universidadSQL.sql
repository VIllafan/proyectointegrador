-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.21 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para universidad
CREATE DATABASE IF NOT EXISTS `universidad` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `universidad`;

-- Volcando estructura para tabla universidad.alumno
CREATE TABLE IF NOT EXISTS `alumno` (
  `idalumno` int NOT NULL AUTO_INCREMENT,
  `matricula` varchar(45) DEFAULT NULL,
  `paterno` varchar(45) DEFAULT NULL,
  `materno` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `correo` varchar(45) NOT NULL,
  `celular` varchar(45) DEFAULT NULL,
  `curp` varchar(45) DEFAULT NULL,
  `foto` mediumtext,
  `fechaNacimiento` datetime DEFAULT NULL,
  `idcredencial` int NOT NULL,
  `idcarrera` int NOT NULL,
  `idgrupo` int NOT NULL,
  `idDireccion` int NOT NULL,
  PRIMARY KEY (`idalumno`),
  KEY `fk_alumno_credencial1_idx` (`idcredencial`),
  KEY `fk_alumno_carrera1_idx` (`idcarrera`),
  KEY `fk_alumno_grupo1_idx` (`idgrupo`),
  KEY `fk_alumno_direccion1_idx` (`idDireccion`),
  CONSTRAINT `fk_alumno_carrera1` FOREIGN KEY (`idcarrera`) REFERENCES `carrera` (`idcarrera`),
  CONSTRAINT `fk_alumno_credencial1` FOREIGN KEY (`idcredencial`) REFERENCES `credencial` (`idcredencial`),
  CONSTRAINT `fk_alumno_direccion1` FOREIGN KEY (`idDireccion`) REFERENCES `direccion` (`idDireccion`),
  CONSTRAINT `fk_alumno_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.alumno: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.alumnoestatus
CREATE TABLE IF NOT EXISTS `alumnoestatus` (
  `idAlumnoEstatus` int NOT NULL AUTO_INCREMENT,
  `idestatus` int NOT NULL,
  `idalumno` int NOT NULL,
  PRIMARY KEY (`idAlumnoEstatus`),
  KEY `fk_AlumnoEstatus_estatus1_idx` (`idestatus`),
  KEY `fk_AlumnoEstatus_alumno1_idx` (`idalumno`),
  CONSTRAINT `fk_AlumnoEstatus_alumno1` FOREIGN KEY (`idalumno`) REFERENCES `alumno` (`idalumno`),
  CONSTRAINT `fk_AlumnoEstatus_estatus1` FOREIGN KEY (`idestatus`) REFERENCES `estatus` (`idestatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.alumnoestatus: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `alumnoestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumnoestatus` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.archivo
CREATE TABLE IF NOT EXISTS `archivo` (
  `idarchivo` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `comentarios` varchar(45) DEFAULT NULL,
  `idtipoArchivo` int NOT NULL,
  `idcargaBitacora` int NOT NULL,
  PRIMARY KEY (`idarchivo`),
  KEY `fk_archivo_tipoArchivo1_idx` (`idtipoArchivo`),
  KEY `fk_archivo_cargaBitacora1_idx` (`idcargaBitacora`),
  CONSTRAINT `fk_archivo_cargaBitacora1` FOREIGN KEY (`idcargaBitacora`) REFERENCES `cargabitacora` (`idcargaBitacora`),
  CONSTRAINT `fk_archivo_tipoArchivo1` FOREIGN KEY (`idtipoArchivo`) REFERENCES `tipoarchivo` (`idtipoArchivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.archivo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `archivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `archivo` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.archivocontenido
CREATE TABLE IF NOT EXISTS `archivocontenido` (
  `idArchivoContenido` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `contenido` text,
  `fechaCarga` datetime DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `idarchivo` int NOT NULL,
  PRIMARY KEY (`idArchivoContenido`),
  KEY `fk_imagen_archivo1_idx` (`idarchivo`),
  CONSTRAINT `fk_imagen_archivo1` FOREIGN KEY (`idarchivo`) REFERENCES `archivo` (`idarchivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.archivocontenido: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `archivocontenido` DISABLE KEYS */;
/*!40000 ALTER TABLE `archivocontenido` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.archivoestatus
CREATE TABLE IF NOT EXISTS `archivoestatus` (
  `idarchivoEstatus` int NOT NULL AUTO_INCREMENT,
  `idarchivo` int NOT NULL,
  `idestatus` int NOT NULL,
  PRIMARY KEY (`idarchivoEstatus`),
  KEY `fk_archivoEstatus_archivo1_idx` (`idarchivo`),
  KEY `fk_archivoEstatus_estatus1_idx` (`idestatus`),
  CONSTRAINT `fk_archivoEstatus_archivo1` FOREIGN KEY (`idarchivo`) REFERENCES `archivo` (`idarchivo`),
  CONSTRAINT `fk_archivoEstatus_estatus1` FOREIGN KEY (`idestatus`) REFERENCES `estatus` (`idestatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.archivoestatus: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `archivoestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `archivoestatus` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.aula
CREATE TABLE IF NOT EXISTS `aula` (
  `idaula` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `idedificio` int NOT NULL,
  `idtipoaula` int NOT NULL,
  PRIMARY KEY (`idaula`),
  KEY `fk_aula_edificio1_idx` (`idedificio`),
  KEY `fk_aula_tipoaula1_idx` (`idtipoaula`),
  CONSTRAINT `fk_aula_edificio1` FOREIGN KEY (`idedificio`) REFERENCES `edificio` (`idedificio`),
  CONSTRAINT `fk_aula_tipoaula1` FOREIGN KEY (`idtipoaula`) REFERENCES `tipoaula` (`idtipoaula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.aula: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `aula` DISABLE KEYS */;
/*!40000 ALTER TABLE `aula` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.bitacoracalificacion
CREATE TABLE IF NOT EXISTS `bitacoracalificacion` (
  `idBitacoraCalificacion` int NOT NULL AUTO_INCREMENT,
  `fechaModificacion` datetime DEFAULT NULL,
  `detalle` varchar(255) DEFAULT NULL,
  `idcalificacion` int NOT NULL,
  PRIMARY KEY (`idBitacoraCalificacion`),
  KEY `fk_BitacoraCalificacion_calificacion1_idx` (`idcalificacion`),
  CONSTRAINT `fk_BitacoraCalificacion_calificacion1` FOREIGN KEY (`idcalificacion`) REFERENCES `calificacion` (`idcalificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.bitacoracalificacion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bitacoracalificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `bitacoracalificacion` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.calificacion
CREATE TABLE IF NOT EXISTS `calificacion` (
  `idcalificacion` int NOT NULL AUTO_INCREMENT,
  `valor` int DEFAULT NULL,
  `valorDecimal` double DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `semestre` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `idusuario` int NOT NULL,
  `idmateria` int NOT NULL,
  `iddocente` int NOT NULL,
  PRIMARY KEY (`idcalificacion`),
  KEY `fk_calificacion_alumno_idx` (`idusuario`),
  KEY `fk_calificacion_materia1_idx` (`idmateria`),
  CONSTRAINT `fk_calificacion_alumno` FOREIGN KEY (`idusuario`) REFERENCES `alumno` (`idalumno`),
  CONSTRAINT `fk_calificacion_materia1` FOREIGN KEY (`idmateria`) REFERENCES `materia` (`idmateria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.calificacion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `calificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `calificacion` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.calificacionestatus
CREATE TABLE IF NOT EXISTS `calificacionestatus` (
  `idcalificacionEstatus` int NOT NULL AUTO_INCREMENT,
  `idestatus` int NOT NULL,
  `idcalificacion` int NOT NULL,
  PRIMARY KEY (`idcalificacionEstatus`),
  KEY `fk_calificacionEstatus_estatus1_idx` (`idestatus`),
  KEY `fk_calificacionEstatus_calificacion1_idx` (`idcalificacion`),
  CONSTRAINT `fk_calificacionEstatus_calificacion1` FOREIGN KEY (`idcalificacion`) REFERENCES `calificacion` (`idcalificacion`),
  CONSTRAINT `fk_calificacionEstatus_estatus1` FOREIGN KEY (`idestatus`) REFERENCES `estatus` (`idestatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.calificacionestatus: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `calificacionestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `calificacionestatus` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.carga
CREATE TABLE IF NOT EXISTS `carga` (
  `idcarga` int NOT NULL AUTO_INCREMENT,
  `fechaCarga` datetime DEFAULT NULL,
  PRIMARY KEY (`idcarga`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.carga: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `carga` DISABLE KEYS */;
/*!40000 ALTER TABLE `carga` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.cargabitacora
CREATE TABLE IF NOT EXISTS `cargabitacora` (
  `idcargaBitacora` int NOT NULL AUTO_INCREMENT,
  `fechaCarga` varchar(45) DEFAULT NULL,
  `comentarios` varchar(45) DEFAULT NULL,
  `idusuario` int NOT NULL,
  PRIMARY KEY (`idcargaBitacora`),
  KEY `fk_cargaBitacora_alumno1_idx` (`idusuario`),
  CONSTRAINT `fk_cargaBitacora_alumno1` FOREIGN KEY (`idusuario`) REFERENCES `alumno` (`idalumno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.cargabitacora: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cargabitacora` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargabitacora` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.cargabitacoraestatus
CREATE TABLE IF NOT EXISTS `cargabitacoraestatus` (
  `idcargaBitacoraEstatus` int NOT NULL AUTO_INCREMENT,
  `cargaBitacora_idcargaBitacora` int NOT NULL,
  `idestatus` int NOT NULL,
  PRIMARY KEY (`idcargaBitacoraEstatus`),
  KEY `fk_cargaBitacoraEstatus_cargaBitacora1_idx` (`cargaBitacora_idcargaBitacora`),
  KEY `fk_cargaBitacoraEstatus_estatus1_idx` (`idestatus`),
  CONSTRAINT `fk_cargaBitacoraEstatus_cargaBitacora1` FOREIGN KEY (`cargaBitacora_idcargaBitacora`) REFERENCES `cargabitacora` (`idcargaBitacora`),
  CONSTRAINT `fk_cargaBitacoraEstatus_estatus1` FOREIGN KEY (`idestatus`) REFERENCES `estatus` (`idestatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.cargabitacoraestatus: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cargabitacoraestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargabitacoraestatus` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.carrera
CREATE TABLE IF NOT EXISTS `carrera` (
  `idcarrera` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcarrera`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.carrera: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `carrera` DISABLE KEYS */;
/*!40000 ALTER TABLE `carrera` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.clase
CREATE TABLE IF NOT EXISTS `clase` (
  `idclase` int NOT NULL AUTO_INCREMENT,
  `key` varchar(45) DEFAULT NULL,
  `idgrupo` int NOT NULL,
  `idmateria` int NOT NULL,
  `idaula` int NOT NULL,
  `iddocente` int NOT NULL,
  PRIMARY KEY (`idclase`),
  KEY `fk_clase_grupo1_idx` (`idgrupo`),
  KEY `fk_clase_materia1_idx` (`idmateria`),
  KEY `fk_clase_aula1_idx` (`idaula`),
  KEY `fk_clase_docente1_idx` (`iddocente`),
  CONSTRAINT `fk_clase_aula1` FOREIGN KEY (`idaula`) REFERENCES `aula` (`idaula`),
  CONSTRAINT `fk_clase_docente1` FOREIGN KEY (`iddocente`) REFERENCES `docente` (`iddocente`),
  CONSTRAINT `fk_clase_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`),
  CONSTRAINT `fk_clase_materia1` FOREIGN KEY (`idmateria`) REFERENCES `materia` (`idmateria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.clase: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clase` DISABLE KEYS */;
/*!40000 ALTER TABLE `clase` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.credencial
CREATE TABLE IF NOT EXISTS `credencial` (
  `idcredencial` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `activo` tinyint DEFAULT NULL,
  `rol_idrol` int NOT NULL,
  PRIMARY KEY (`idcredencial`),
  KEY `fk_credencial_rol1_idx` (`rol_idrol`),
  CONSTRAINT `fk_credencial_rol1` FOREIGN KEY (`rol_idrol`) REFERENCES `rol` (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.credencial: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `credencial` DISABLE KEYS */;
INSERT INTO `credencial` (`idcredencial`, `username`, `password`, `activo`, `rol_idrol`) VALUES
	(1, 'analalistaMex01@dgp.com', '123', 1, 1);
/*!40000 ALTER TABLE `credencial` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.departamento
CREATE TABLE IF NOT EXISTS `departamento` (
  `iddepartamento` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `key` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iddepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.departamento: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.direccion
CREATE TABLE IF NOT EXISTS `direccion` (
  `idDireccion` int NOT NULL AUTO_INCREMENT,
  `calle` varchar(45) DEFAULT NULL,
  `colonia` varchar(45) DEFAULT NULL,
  `lote` varchar(45) DEFAULT NULL,
  `manzana` varchar(45) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `municipio` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `codigoPostal` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idDireccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.direccion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.docente
CREATE TABLE IF NOT EXISTS `docente` (
  `iddocente` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `materno` varchar(45) DEFAULT NULL,
  `paterno` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `matricula` varchar(45) DEFAULT NULL,
  `foto` text,
  `idcredencial` int NOT NULL,
  `idDireccion` int NOT NULL,
  PRIMARY KEY (`iddocente`),
  KEY `fk_docente_credencial1_idx` (`idcredencial`),
  KEY `fk_docente_direccion1_idx` (`idDireccion`),
  CONSTRAINT `fk_docente_credencial1` FOREIGN KEY (`idcredencial`) REFERENCES `credencial` (`idcredencial`),
  CONSTRAINT `fk_docente_direccion1` FOREIGN KEY (`idDireccion`) REFERENCES `direccion` (`idDireccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.docente: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `docente` DISABLE KEYS */;
/*!40000 ALTER TABLE `docente` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.edificio
CREATE TABLE IF NOT EXISTS `edificio` (
  `idedificio` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `ubicacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idedificio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.edificio: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `edificio` DISABLE KEYS */;
/*!40000 ALTER TABLE `edificio` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.estatus
CREATE TABLE IF NOT EXISTS `estatus` (
  `idestatus` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `key` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idestatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.estatus: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `estatus` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.fechasbase
CREATE TABLE IF NOT EXISTS `fechasbase` (
  `idfechasBase` int NOT NULL AUTO_INCREMENT,
  `key` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `fechaInicio` datetime DEFAULT NULL,
  `fechaFin` datetime DEFAULT NULL,
  PRIMARY KEY (`idfechasBase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.fechasbase: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `fechasbase` DISABLE KEYS */;
/*!40000 ALTER TABLE `fechasbase` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.grupo
CREATE TABLE IF NOT EXISTS `grupo` (
  `idgrupo` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `key` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.grupo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.horario
CREATE TABLE IF NOT EXISTS `horario` (
  `idhorario` int NOT NULL AUTO_INCREMENT,
  `horaInicio` datetime DEFAULT NULL,
  `horaFin` datetime DEFAULT NULL,
  `dia` varchar(45) DEFAULT NULL,
  `idclase` int NOT NULL,
  PRIMARY KEY (`idhorario`),
  KEY `fk_horario_clase1_idx` (`idclase`),
  CONSTRAINT `fk_horario_clase1` FOREIGN KEY (`idclase`) REFERENCES `clase` (`idclase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.horario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.materia
CREATE TABLE IF NOT EXISTS `materia` (
  `idmateria` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `key` varchar(45) DEFAULT NULL,
  `creditos` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idmateria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.materia: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `materia` DISABLE KEYS */;
/*!40000 ALTER TABLE `materia` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.rol
CREATE TABLE IF NOT EXISTS `rol` (
  `idrol` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `key` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.rol: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` (`idrol`, `nombre`, `key`) VALUES
	(1, 'ANALISTAMEX', 'AMEX'),
	(2, 'ANALISTAMEX', 'AMEX'),
	(3, 'ANALISTAMEX', 'AMEX'),
	(4, 'ANALISTAMEX', 'AMEX'),
	(5, 'ANALISTAMEX', 'AMEX');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.tipoarchivo
CREATE TABLE IF NOT EXISTS `tipoarchivo` (
  `idtipoArchivo` int NOT NULL AUTO_INCREMENT,
  `key` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipoArchivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.tipoarchivo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipoarchivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoarchivo` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.tipoaula
CREATE TABLE IF NOT EXISTS `tipoaula` (
  `idtipoaula` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipoaula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.tipoaula: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipoaula` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoaula` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.tramite
CREATE TABLE IF NOT EXISTS `tramite` (
  `idtramite` int NOT NULL AUTO_INCREMENT,
  `key` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idtramite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.tramite: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tramite` DISABLE KEYS */;
/*!40000 ALTER TABLE `tramite` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.tramiteestatus
CREATE TABLE IF NOT EXISTS `tramiteestatus` (
  `idtramiteEstatus` int NOT NULL AUTO_INCREMENT,
  `idestatus` int NOT NULL,
  `idtramite` int NOT NULL,
  PRIMARY KEY (`idtramiteEstatus`),
  KEY `fk_tramiteEstatus_estatus1_idx` (`idestatus`),
  KEY `fk_tramiteEstatus_tramite1_idx` (`idtramite`),
  CONSTRAINT `fk_tramiteEstatus_estatus1` FOREIGN KEY (`idestatus`) REFERENCES `estatus` (`idestatus`),
  CONSTRAINT `fk_tramiteEstatus_tramite1` FOREIGN KEY (`idtramite`) REFERENCES `tramite` (`idtramite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.tramiteestatus: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tramiteestatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `tramiteestatus` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.tramitetipoarchivo
CREATE TABLE IF NOT EXISTS `tramitetipoarchivo` (
  `idtramiteTipoArchivo` int NOT NULL AUTO_INCREMENT,
  `idtramite` int NOT NULL,
  `idarchivo` int NOT NULL,
  PRIMARY KEY (`idtramiteTipoArchivo`),
  KEY `fk_tramiteTipoArchivo_tramite1_idx` (`idtramite`),
  KEY `fk_tramiteTipoArchivo_archivo1_idx` (`idarchivo`),
  CONSTRAINT `fk_tramiteTipoArchivo_archivo1` FOREIGN KEY (`idarchivo`) REFERENCES `archivo` (`idarchivo`),
  CONSTRAINT `fk_tramiteTipoArchivo_tramite1` FOREIGN KEY (`idtramite`) REFERENCES `tramite` (`idtramite`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.tramitetipoarchivo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tramitetipoarchivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tramitetipoarchivo` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `idusuario` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `paterno` varchar(45) DEFAULT NULL,
  `materno` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `foto` text,
  `iddepartamento` int NOT NULL,
  `idDireccion` int NOT NULL,
  `idcredencial` int NOT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_credencial1_idx` (`idcredencial`),
  KEY `fk_usuario_departamento1_idx` (`iddepartamento`),
  KEY `fk_usuario_direccion1_idx` (`idDireccion`),
  CONSTRAINT `fk_usuario_credencial1` FOREIGN KEY (`idcredencial`) REFERENCES `credencial` (`idcredencial`),
  CONSTRAINT `fk_usuario_departamento1` FOREIGN KEY (`iddepartamento`) REFERENCES `departamento` (`iddepartamento`),
  CONSTRAINT `fk_usuario_direccion1` FOREIGN KEY (`idDireccion`) REFERENCES `direccion` (`idDireccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla universidad.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
